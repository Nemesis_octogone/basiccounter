﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Decrement = New System.Windows.Forms.Button()
        Me.Increment = New System.Windows.Forms.Button()
        Me.Rezero = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Decrement
        '
        Me.Decrement.Location = New System.Drawing.Point(116, 118)
        Me.Decrement.Name = "Decrement"
        Me.Decrement.Size = New System.Drawing.Size(125, 90)
        Me.Decrement.TabIndex = 0
        Me.Decrement.Text = "-"
        Me.Decrement.UseVisualStyleBackColor = True
        '
        'Increment
        '
        Me.Increment.Location = New System.Drawing.Point(577, 118)
        Me.Increment.Name = "Increment"
        Me.Increment.Size = New System.Drawing.Size(125, 90)
        Me.Increment.TabIndex = 1
        Me.Increment.Text = "+"
        Me.Increment.UseVisualStyleBackColor = True
        '
        'Rezero
        '
        Me.Rezero.Location = New System.Drawing.Point(337, 292)
        Me.Rezero.Name = "Rezero"
        Me.Rezero.Size = New System.Drawing.Size(116, 67)
        Me.Rezero.TabIndex = 2
        Me.Rezero.Text = "RAZ"
        Me.Rezero.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(359, 34)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 31)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Total"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(379, 142)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(29, 31)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "0"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Rezero)
        Me.Controls.Add(Me.Increment)
        Me.Controls.Add(Me.Decrement)
        Me.Name = "Form1"
        Me.Text = "BasicCounter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Decrement As Button
    Friend WithEvents Increment As Button
    Friend WithEvents Rezero As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
End Class
