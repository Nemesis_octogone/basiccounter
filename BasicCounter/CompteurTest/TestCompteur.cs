﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using BasicCounterLibrary;

namespace CompteurTest
{
    [TestClass]
    public class TestCompteur
    {
        [TestMethod]
        public void TestIncrementation()
        {
            CompteurClass.Incrementation();
            Assert.AreEqual(1, CompteurClass.Compteur);
        }
        public void TestDecrementation()
        {
            CompteurClass.Decrementation();
            Assert.AreEqual(0, CompteurClass.Compteur);
        }
        public void TestRAZ()
        {
            CompteurClass.RAZ();
            Assert.AreEqual(0, CompteurClass.Compteur);
        }
    }
}
